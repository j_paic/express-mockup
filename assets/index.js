export { default as home1 } from './home_1.png';
export { default as home2 } from './home_2.png';
export { default as logo } from './logotrans.png';

export { default as banner1 } from './category_1.png';
export { default as banner2 } from './category_2.png';
export { default as banner3 } from './category_3.png';
export { default as banner4 } from './category_4.png';

export { default as cart } from './express-cart.png';

export { default as notificaiton } from './notification.png';

export { default as item1 } from './express_image1.png';
export { default as item2 } from './express_image2.png';
export { default as item3 } from './express_image3.png';
export { default as item4 } from './express_image4.png';
export { default as item5 } from './express_image5.png';
export { default as item6 } from './express_image6.png';
export { default as item7 } from './express_image7.png';
export { default as item8 } from './express_image8.png';
export { default as item9 } from './express_image9.png';

export { default as homeIcon } from './booking.png';

export { default as express1 } from './express1.jpg';
export { default as express2 } from './express2.png';
export { default as express3 } from './express3.jpg';
export { default as express4 } from './express4.jpg';
