import React from 'react';
import { ThemeProvider } from 'styled-components';
import AppNavigator from './src/Routes';
import theme from './src/theme';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <AppNavigator />
    </ThemeProvider>
  );
};
export default App;
