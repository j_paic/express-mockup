export default {
  color: {
    background: '#FCFCFC',
    base: '#E65A31',
    darkbase: '#C83001',
    lightgray: '#dfdfdf',
  },
  statusbar: {
    light: '#ECECEC',
    base: '#C83001',
  },
  header: {
    background: '#E65A31',
  },
  text: {
    light: '#8A8A8A',
    dark: '#404040',
    black: '#060606',
    copyright: '#014C5A',
  },
  line: {
    color: '#C2C2C2',
  },
  drawer: {
    textcolor: '#8A8A8A',
  },
};
