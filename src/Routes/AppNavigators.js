import { createAppContainer, createStackNavigator } from 'react-navigation';
import theme from '../theme';
import Homepage from '../screens/Mainpage';

const App = createStackNavigator(
  {
    Home: Homepage,
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: theme.color.base,
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
      },
      headerTintColor: theme.header.background,
    }),
  },
);

export default createAppContainer(App);
