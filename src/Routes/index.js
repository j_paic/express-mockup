import { createAppContainer, createStackNavigator } from 'react-navigation';
import theme from '../theme';
import MainPage from '../screens/Main';
import HomePage from '../screens/Homepage';
import ProductView from '../screens/ProductView';
import TrackingPackage from '../screens/TrackPackageView';
import ProductDetails from '../screens/ProductDetails';

const App = createStackNavigator(
  {
    Main: MainPage,
    Home: HomePage,
    Product: ProductView,
    Tracking: TrackingPackage,
    ProductDetails,
  },
  {
    initialRouteName: 'Main',
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: theme.color.base,
        shadowColor: 'transparent',
        shadowOpacity: 0,
        borderBottomWidth: 0,
      },
      headerTintColor: theme.header.background,
    }),
  },
);

export default createAppContainer(App);
