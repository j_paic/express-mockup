import React from 'react';
import { View, Image } from 'react-native';
import { Icon } from 'native-base';
import { logo } from '../../../assets';

export default function HeaderLeft() {
  return (
    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
      <Icon
        name="md-menu"
        style={{
          color: 'white',
          marginRight: 15,
        }}
      />
      <Image
        source={logo}
        style={{
          width: 30,
          height: 30,
        }}
      />
    </View>
  );
}
