import React from 'react';
import { View, Image } from 'react-native';
import { Icon } from 'native-base';
import { cart, notificaiton } from '../../../assets';

export default function HeaderRight() {
  return (
    <View
      style={{
        flexDirection: 'row',
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Icon
        name="pricetag"
        style={{
          color: 'white',
          marginRight: 15,
          fontSize: 20,
        }}
      />
      <Icon
        name="search"
        style={{
          color: 'white',
          marginRight: 15,
          fontSize: 20,
        }}
      />
      <Image
        source={notificaiton}
        style={{
          width: 18,
          height: 18,
          marginRight: 15,
        }}
      />
      <Image
        source={cart}
        style={{
          width: 20,
          height: 20,
        }}
      />
    </View>
  );
}
