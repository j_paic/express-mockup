import React from 'react';
import styled from 'styled-components';

const HeaderTitleView = styled.View`
  height: 54px;
  justify-content: center;
  flex-direction: column;
  padding-left: 40px;
  background-color: ${({ theme }) => theme.color.base};
`;

const HeaderText = styled.Text`
  color: white;
  font-weight: normal;
  font-size: 15px;
`;

export default function TitleHeader() {
  return (
    <HeaderTitleView>
      <HeaderText>!Hola!</HeaderText>
      <HeaderText>Elegir Tu Servicio</HeaderText>
    </HeaderTitleView>
  );
}
