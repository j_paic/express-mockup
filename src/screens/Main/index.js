import React from 'react';
import {
  FlatList,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { Icon } from 'native-base';
import { home1, home2, logo } from '../../../assets';
import { Container } from './styl';
import TitleHeader from './TitleComponent';

const { height, width } = Dimensions.get('window');
// create the image data to display flatlist
const imageData = [
  {
    key: 'key-1',
    Image: home1,
    path: 'Home',
  },
  {
    key: 'key-2',
    Image: home2,
    path: 'Home',
  },
];

const BannerItem = ({ image, path, onPress }) => {
  return (
    <View
      style={{
        height: height - 170,
        width: width - 30,
        justifyContent: 'center',
      }}
    >
      <TouchableWithoutFeedback onPress={() => onPress(path)}>
        <Image
          source={image}
          resizeMode="contain"
          style={{
            height: null,
            flex: 1,
            width: width - 40,
            alignSelf: 'center',
          }}
        />
      </TouchableWithoutFeedback>
    </View>
  );
};

const HORIZONTAL_ITEM_WIDTH = 200;
const LIST_HEIGHT = 160;
const contentMargin = (width - HORIZONTAL_ITEM_WIDTH) / 2;
const contentStyle = {
  paddingLeft: -contentMargin,
  paddingRight: contentMargin,
};

const itemLayout = (item, index) => ({
  length: HORIZONTAL_ITEM_WIDTH,
  offset: index * HORIZONTAL_ITEM_WIDTH,
  index,
});

const HomePageView = ({ navigation }) => {
  return (
    <Container>
      <TitleHeader />
      <FlatList
        horizontal
        pagingEnabled
        style={{ flex: 1 }}
        contentContainerStyle={contentStyle}
        decelerationRate="fast"
        // getItemLayout={itemLayout}
        keyboardShouldPersistTaps="handled"
        data={imageData}
        renderItem={({ item }) => (
          <BannerItem
            image={item.Image}
            path={item.path}
            onPress={(path) => navigation.navigate(path)}
          />
        )}
        removeClippedSubviews={false}
        ItemSeparatorComponent={() => (
          <View
            style={{
              width: 1,
              backgroundColor: '#e0e0e0',
              height: height * 0.65,
              marginTop: 50,
            }}
          />
        )}
      />
    </Container>
  );
};

HomePageView.navigationOptions = ({ navigation }) => {
  return {
    headerLeft: (
      <View style={{ flexDirection: 'row', marginLeft: 10 }}>
        <Icon
          name="md-menu"
          style={{
            color: 'white',
            marginRight: 15,
          }}
        />
        <Image
          source={logo}
          style={{
            width: 30,
            height: 30,
          }}
        />
      </View>
    ),
  };
};

export default HomePageView;
