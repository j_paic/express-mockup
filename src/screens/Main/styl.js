import styled from 'styled-components';

export const Container = styled.View`
  flex: 1;
  /* padding: 2px; */
  /* background-color: yellow; */
`;

export const ImageItemContainer = styled.View`
  flex: 1;
  background-color: red;
  justify-content: center;
  align-items: center;
  width: ${({ width }) => width || 100};
  height: ${({ height }) => height || 100};
`;
