import React from 'react';
import { Text, View, Image, Dimensions } from 'react-native';
import styled from 'styled-components';
import HeaderLeft from '../Components/HeaderLeft';
import HeaderRight from '../Components/HeaderRight';

import { express1 } from '../../../assets';

const { width, height } = Dimensions.get('window');

const ProductDetailsBox = styled.View`
  flex: 1;
`;

const NavBar = styled.View`
  background-color: ${({ theme }) => theme.color.base};
  height: 30;
`;

function ProductDetails() {
  return (
    <ProductDetailsBox>
      <NavBar>
        <Text style={{ color: '#FFFFFF', textAlign: 'center' }}>
          Detalles del Producto
        </Text>
      </NavBar>
      <View
        style={{
          height: height / 4,
          paddingHorizontal: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Image
          source={express1}
          resizeMode="contain"
          style={{ width: width - 16 }}
        />
      </View>
      <View style={{ marginHorizontal: 10 }}>
        <Text>Vaso de Acero Inoxidable CHILLOUT LIFE</Text>
        <Text>By Sompopo</Text>
      </View>
    </ProductDetailsBox>
  );
}

ProductDetails.navigationOptions = () => ({
  headerLeft: <HeaderLeft />,
  headerRight: <HeaderRight />,
});

export default ProductDetails;
