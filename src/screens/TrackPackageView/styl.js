import styled from 'styled-components';

export const Container = styled.ScrollView`
  flex: 1;
`;

export const HeaderText = styled.Text`
  font-size: 15;
  padding-left: 15px;
  padding-bottom: 10;
  padding-top: 20;
  color: ${({ theme }) => theme.text.light};
`;

export const TotalOrderView = styled.View`
  flex-direction: row;
  padding-left: 15px;
  padding-right: 15px;
  justify-content: space-between;
`;
export const OrderText = styled.Text`
  color: ${({ theme }) => theme.text.light};
`;

export const TotalCostText = styled.Text`
  color: ${({ theme }) => theme.text.light};
`;

export const ETAText = styled.Text`
  font-size: 18px;
  color: #000000;
  padding-left: 15px;
  padding-top: 20px;
  font-weight: bold;
`;
