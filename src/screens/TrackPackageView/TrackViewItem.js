import React from 'react';
import { StyleSheet } from 'react-native';
import styled from 'styled-components';

const CircleView = styled.View`
  width: 14;
  height: 14;
  border-radius: 7;
  background-color: ${({ color, theme }) => color || theme.color.base};
`;
const TrackViewContainer = styled.View`
  margin-left: 10;
  flex-direction: row;
`;
const BodyView = styled.View`
  flex-direction: column;
  flex: 1;
  padding-left: 10;
`;
const DottedLineView = styled.View`
  /* justify-content: center; */
  align-items: center;
  margin-top: 3px;
`;
const DottedLine = styled.View`
  flex: 1;
  border-width: ${StyleSheet.hairlineWidth};
  border-style: dashed;
  width: 1px;
  /* background-color: ${({ theme }) => theme.color.base}; */
  border-color: ${({ theme }) => theme.color.base};
`;

const DetialView = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-around;
`;
const Title = styled.Text`
  color: ${({ theme }) => theme.color.base};
  font-size: 16;
  font-weight: bold;
`;
const DescriptionView = styled.View`
  flex: 1;
  padding-bottom: 25px;
  padding-top: 5;
`;
const DescriptionText = styled.Text`
  font-size: 12px;
  color: ${({ theme }) => theme.text.dark};
  text-align: left;
`;

const TimeView = styled.View`
  width: 70px;
  margin-right: 10px;
`;

const TimeText = styled.Text`
  font-size: 12px;
  color: ${({ theme }) => theme.text.dark};
  text-align: right;
`;

const TrackingViewItem = ({ item, index }) => {
  const { title, description, time } = item;

  return (
    <TrackViewContainer>
      <DottedLineView>
        <CircleView color={index === 0 ? 'green' : null} />
        {index === 3 ? null : <DottedLine />}
      </DottedLineView>

      <BodyView>
        <Title>{title}</Title>
        <DetialView>
          <DescriptionView>
            <DescriptionText>{description}</DescriptionText>
          </DescriptionView>
          <TimeView>
            <TimeText>{time}</TimeText>
          </TimeView>
        </DetialView>
      </BodyView>
    </TrackViewContainer>
  );
};

export default TrackingViewItem;
