import React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { homeIcon } from '../../../assets';

const { width } = Dimensions.get('window');

function DeliveryAddress() {
  const size = width / 4;
  const imagesize = width / 8;
  return (
    <View style={{ flexDirection: 'row' }}>
      <View
        style={{
          width: size,
          height: size,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Image
          source={homeIcon}
          resizeMethod="scale"
          resizeMode="contain"
          style={{
            height: imagesize,
            width: imagesize,
          }}
        />
      </View>
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Text
          style={{
            fontSize: 16,
            color: 'black',
            fontWeight: 'bold',
            paddingBottom: 6,
          }}
        >
          Delivery Address:
        </Text>
        <Text>Blvd. Suyapa, Torre Metropolis 1, Piso 18.</Text>
      </View>
    </View>
  );
}

export default DeliveryAddress;
