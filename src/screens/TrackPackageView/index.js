import React from 'react';
import { FlatList } from 'react-native';
import {
  Container,
  HeaderText,
  TotalOrderView,
  OrderText,
  TotalCostText,
  ETAText,
} from './styl';
import trackingdata from './trackdingdata';
import TrackingViewItem from './TrackViewItem';
import HeaderLeft from '../Components/HeaderLeft';
import HeaderRight from '../Components/HeaderRight';
import DeliveryAddress from './DeliveryAddress';

function TrackPackage() {
  return (
    <Container>
      <HeaderText>Lunes, 11 de Marzo</HeaderText>
      <TotalOrderView>
        <OrderText>Order Id: askcneifne 1145</OrderText>
        <TotalCostText>Total L. 1750</TotalCostText>
      </TotalOrderView>
      <ETAText>ETA : 30 min.</ETAText>
      <FlatList
        style={{ height: 350, marginTop: 20, marginRight: 10 }}
        data={trackingdata}
        renderItem={({ item, index }) => (
          <TrackingViewItem item={item} index={index} />
        )}
      />
      <DeliveryAddress />
    </Container>
  );
}

TrackPackage.navigationOptions = () => ({
  headerLeft: <HeaderLeft />,
  headerRight: <HeaderRight />,
});

export default TrackPackage;
