const trackdata = [
  {
    key: '1',
    title: 'Order Received',
    description: 'Order has been received and confirmed',
    time: '8:00pm',
  },
  {
    key: '2',
    title: 'Order is being prepared',
    description:
      'We are preparing your order to be shipped. An Agente Sompopo will deliver your package shortly',
    time: '8:10pm',
  },
  {
    key: '3',
    title: "Package is on it's way",
    description: "Agente Sompopo is on it's way to deliver your package",
    time: '8:30pm',
  },
  {
    key: '4',
    title: 'Order delivered',
    description:
      'Order is delivered to your home. Thank you for choosing Sompopo Express.',
    time: '8:40pm',
  },
];

export default trackdata;
