import React from 'react';
import { TouchableWithoutFeedback, View, Text } from 'react-native';
import {
  ProductItemView,
  RenderImage,
  BuyButton,
  PriceText,
  ProductNameText,
} from './styl';

function ProductItem({ item, onPress, onButtonPress, tobuy }) {
  let boughtItems;
  if (tobuy) {
    boughtItems = tobuy[item.key];
  }
  return (
    <ProductItemView>
      <TouchableWithoutFeedback onPress={() => onPress(item.key)}>
        <RenderImage source={item.image} resizeMode="contain" />
      </TouchableWithoutFeedback>
      {boughtItems && (
        <View
          style={{
            position: 'absolute',
            right: 5,
            top: 5,
            width: 20,
            height: 20,
            borderRadius: 10,
            backgroundColor: '#cecece',
            justifyContent: 'center',
          }}
        >
          <Text style={{ textAlign: 'center', fontSize: 9 }}>
            {boughtItems}
          </Text>
        </View>
      )}
      <ProductNameText numberOfLines={2}>{item.name}</ProductNameText>
      <PriceText>{item.price}</PriceText>
      <BuyButton onPress={() => onButtonPress(item.key)} />
    </ProductItemView>
  );
}

export default ProductItem;
