import React, { useState } from 'react';
import {
  FlatList,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import styled from 'styled-components';
import { categorydata } from './data';

const { width } = Dimensions.get('window');

const CatView = styled.View``;
const CategoryItem = styled.View`
  height: 35;
  background-color: ${({ color, theme }) => color || theme.color.base};
  justify-content: center;
  align-items: center;
  width: ${width / 5};
`;
const CategoryText = styled.Text`
  color: white;
  text-align: center;
  padding-horizontal: 10;
`;

const LineView = styled.View`
  width: ${StyleSheet.hairlineWidth};
  background-color: gray;
`;

export default function ProductCategory() {
  const [selected, setSelected] = useState(0);
  return (
    <CatView>
      <FlatList
        horizontal
        extraData={selected}
        data={categorydata}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            onPress={() => {
              console.log(item, index);
              setSelected(index);
            }}
          >
            <CategoryItem color={selected === index ? 'green' : null}>
              <CategoryText index={index}>{item}</CategoryText>
            </CategoryItem>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => <LineView />}
      />
    </CatView>
  );
}
