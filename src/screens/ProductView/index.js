import React, { useState } from 'react';
import { FlatList } from 'react-native';
import { Fab, Button, Icon } from 'native-base';
import data from './data';
import HeaderLeft from '../Components/HeaderLeft';
import HeaderRight from '../Components/HeaderRight';
import { ProductViewContainer } from './styl';
import ProductItem from './ProductItem';
import ProductCategory from './ProductCategory';

function ProductGridViewPage({ navigation }) {
  const [selected, setSelected] = useState(0);
  const [active, setActive] = useState(false);
  const [purchased, setPurchased] = useState({});
  return (
    <ProductViewContainer>
      <ProductCategory />
      <FlatList
        style={{ flex: 1 }}
        extraData={selected}
        data={data}
        numColumns={3}
        keyExtractor={({ key }) => key}
        renderItem={({ item }) => (
          <ProductItem
            item={item}
            selected={selected}
            onPress={(index) => {
              setSelected(index);
              const navview = index == 2 ? 'Tracking' : 'ProductDetails';
              navigation.navigate(navview, { selected });
            }}
            tobuy={purchased}
            onButtonPress={(itemkey) => {
              const md = purchased[itemkey];
              const updatecount = (md || 0) + 1;
              setPurchased(
                Object.assign({}, purchased, { [itemkey]: updatecount }),
              );
            }}
          />
        )}
      />
      <Fab
        active={active}
        direction="up"
        containerStyle={{}}
        style={{ backgroundColor: '#34A34F' }}
        position="bottomRight"
        onPress={() => setActive(!active)}
      >
        <Icon name="funnel" />
        <Button style={{ backgroundColor: '#34A34F' }}>
          <Icon name="logo-whatsapp" />
        </Button>
        <Button style={{ backgroundColor: '#3B5998' }}>
          <Icon name="logo-facebook" />
        </Button>
        <Button disabled style={{ backgroundColor: '#DD5144' }}>
          <Icon name="mail" />
        </Button>
      </Fab>
    </ProductViewContainer>
  );
}

ProductGridViewPage.navigationOptions = () => ({
  headerLeft: <HeaderLeft />,
  headerRight: <HeaderRight />,
});
export default ProductGridViewPage;
