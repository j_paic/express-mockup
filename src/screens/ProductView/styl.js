import React from 'react';
import styled from 'styled-components';
import { Dimensions, TouchableOpacity } from 'react-native';

const { width } = Dimensions.get('window');

export const ProductViewContainer = styled.View`
  background-color: ${({ theme }) => theme.color.lightgray};
  flex: 1;
  margin: 2px;
`;

export const RenderImage = styled.Image`
  flex: 1;
  height: 110;
  width: ${width / 4 + 5};
`;

export const ProductItemView = styled.View`
  height: 250;
  width: ${width / 3 - 9};
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.color.background};
  margin: 3px;
  padding-bottom: 5;
`;

const ButtonView = styled.View`
  background-color: ${({ theme }) => theme.color.base};
  height: 30;
  border-radius: 7;
  justify-content: center;
  align-items: center;
  padding-horizontal: 8;
`;
const TextButton = styled.Text`
  color: white;
  font-size: 10;
`;

export const BuyButton = ({ onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <ButtonView>
      <TextButton>Anadir Al Carrito </TextButton>
    </ButtonView>
  </TouchableOpacity>
);

export const PriceText = styled.Text`
  color: ${({ theme }) => theme.color.base};
  text-align: center;
  padding-bottom: 10;
`;

export const ProductNameText = styled.Text`
  font-size: 14;
  text-align: center;
  padding-bottom: 10;
  padding-horizontal: 5;
`;

export const ProductBorderView = styled.View``;
