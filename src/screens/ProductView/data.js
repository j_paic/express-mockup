import {
  item1,
  item2,
  item3,
  item4,
  item5,
  item6,
  item7,
  item8,
  item9,
} from '../../../assets';

const data = [
  { key: '1', image: item1, name: 'Test Name Product', price: 'L 120.99' },
  { key: '2', image: item2, name: 'Test Name Product', price: 'L 120.99' },
  { key: '3', image: item3, name: 'Test Name Product', price: 'L 120.99' },
  { key: '4', image: item4, name: 'Test Name Product', price: 'L 120.99' },
  { key: '5', image: item5, name: 'Test Name Product', price: 'L 120.99' },
  { key: '6', image: item6, name: 'Test Name Product', price: 'L 120.99' },
  { key: '7', image: item7, name: 'Test Name Product', price: 'L 120.99' },
  { key: '8', image: item8, name: 'Test Name Product', price: 'L 120.99' },
  { key: '9', image: item9, name: 'Test Name Product', price: 'L 120.99' },
];

export const categorydata = [
  'All',
  'Burger',
  'Snack',
  'Drink',
  'Pizza',
  'Licros',
];

export default data;
