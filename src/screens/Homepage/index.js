import React from 'react';
import {
  View,
  FlatList,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { banner1, banner2, banner3, banner4 } from '../../../assets';

import HeaderLeft from '../Components/HeaderLeft';
import HeaderRight from '../Components/HeaderRight';

const imagelist = [banner1, banner2, banner3, banner4];

function HomePage({ navigation }) {
  return (
    <View style={{ flex: 1, marginHorizontal: 13 }}>
      <FlatList
        data={imagelist}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => navigation.navigate('Product')}>
            <View style={{ height: 180 }}>
              <Image
                source={item}
                resizeMode="contain"
                style={{ flex: 1, height: 180, width: null }}
              />
            </View>
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={() => (
          <View
            style={{
              height: StyleSheet.hairlineWidth,
              backgroundColor: 'gray',
            }}
          />
        )}
      />
    </View>
  );
}

HomePage.navigationOptions = () => ({
  headerLeft: <HeaderLeft />,
  headerRight: <HeaderRight />,
});
export default HomePage;
